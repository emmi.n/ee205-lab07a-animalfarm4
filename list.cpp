///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file list.cpp
/// @version 1.0
///
/// Exports data about all lists
///
/// @author EmilyPham <emilyn3@hawaii.edu>
/// @brief  Lab 07a - Animal Farm 4 - EE 205 - Spr 2021
/// @date   31_MAR_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include<cassert>

#include "list.hpp"

//#define DEBUG

using namespace std;
namespace containers {

#ifdef DEBUG   // a "quick and dirty" iterator built for testing
void SingleLinkedList::iterator() const{
   Node* current = head;
   for( int i=0; i<3; i++) {
      cout << "PUSH CHECK: next addr =" << current->next << "  current =" << current << endl;
      current = current->next;
   }
}
#endif

const bool SingleLinkedList::empty() const { return ( head == nullptr ); }

void  SingleLinkedList::push_front( Node* newNode ) {
   if( newNode == nullptr ) {
      cout << "nothing added." << endl;   // lets user know nothing happened, returns
      return;
   }
#ifdef DEBUG
   cout << "before push: next addr =" << newNode->next << "  head =" << head << endl;
   cout << "             newNode = " << newNode << endl;
#endif
   newNode->next = head;
   head = newNode;

   numberOFnodes++;
#ifdef DEBUG
   cout << "after push:  next addr = " << newNode->next << " head = " << head << endl;
#endif
}

Node* SingleLinkedList::pop_front() {
   if( head == nullptr ) {
#ifdef DEBUG
      cout << "head pointer is null. nothing to pop." << endl;
#endif
      return head;
   }
#ifdef DEBUG
   cout << "before pop: next addr =" << head->next << "  head =" << head << endl;
#endif  
   Node* oldHead = head;
   head = head->next;
   oldHead->next = nullptr;

   numberOFnodes--;
#ifdef DEBUG
   if ( head == nullptr ) {
      cout << "after pop:  head pointer is null" << endl;
   }else{
      cout << "after pop:  next addr = " << head->next << " head = " << head << endl << endl;
   }
#endif
   return oldHead;
}


Node* SingleLinkedList::get_first() const { return head; }

Node* SingleLinkedList::get_next( const Node* currentNode ) const {
   assert( currentNode != nullptr );
   return currentNode->next;
}

unsigned int SingleLinkedList::size() const { return numberOFnodes; }

} // namespace containers
