///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file nunu.cpp
/// @version 1.0
///
/// Exports data about all nunu fish
///
/// @author Emily_Pham <emilyn3@hawaii.edu>
/// @brief  Lab 07a - Animal Farm 4 - EE 205 - Spr 2021
/// @date   25_MAR_2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "nunu.hpp"

using namespace std;

namespace animalfarm {
	
Nunu::Nunu( bool newIsNative, enum Color newColor, enum Gender newGender ) {
	isNative = newIsNative;          /// all Nunu are native (is-a relationship) but we are passing this info through constructor cause can
   gender = newGender;         
	species = "Fistularia chinensis";
	scaleColor = newColor;
   favoriteTemperature = 80.6;      /// it is bold of you to assume that all my nunu like exactly 80.6 degree temp
}


/// Print our Nunu and that it's native... then print whatever information Fish holds.
void Nunu::printInfo() {
   cout << "Nunu" << endl;
   cout << "   Is native = [" << boolalpha << isNative  << "]" << endl;
	Fish::printInfo();
}

} // namespace animalfarm
