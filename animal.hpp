///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file animal.hpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author EmilyPham <emilyn3@hawaii.edu>
/// @brief  Lab 07a - Animal Farm 4 - EE 205 - Spr 2021
/// @date   25_MAR_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

using namespace std;

#include <string>
#include <random>

#include "node.hpp"

namespace animalfarm {

enum Gender { MALE, FEMALE, UNKNOWN };

enum Color{ BLACK, WHITE, BROWN, SILVER, RED, YELLOW, GREEN, BLUE };

class Animal : public containers::Node {
public:
	enum Gender gender;
	string      species;

public:
   Animal();
   ~Animal();

public:
	virtual const string speak() = 0;
	
	void printInfo();
	
	string colorName  (enum Color color);
	string genderName (enum Gender gender);

   static const Gender getRandomGender();
   static const Color  getRandomColor();
   static const bool   getRandomBool();
   static const float  getRandomWeight( const float from, const float to );
   static const string getRandomName();
};

} // namespace animalfarm
