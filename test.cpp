///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file test.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author EmilyPham <emilyn3@hawaii.edu>
/// @brief  Lab 07a - Animal Farm 4 - EE 205 - Spr 2021
/// @date   29_MAR_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <list>
#include <cassert>

#include "node.hpp"
#include "list.hpp"
#include "animalfactory.hpp"
#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"

using namespace std;
using namespace animalfarm;
using namespace containers;

int main() {
   
   cout << "Welcome to Animal Farm 4" << endl;

   Node node1; 
   Node node2; 
   Node node3;

   SingleLinkedList list; 

   // empty() test
   cout << boolalpha << list.empty() << endl;

   // push_front() and size() test
   cout << "size: " << list.size() << endl;
   list.push_front( &node1 );
   cout << "size: " << list.size() << endl;
   list.push_front( &node2 );
   cout << "size: " << list.size() << endl;
   list.push_front( &node3 );
   cout << "size: " << list.size() << endl;
   cout << boolalpha << list.empty() << endl;
   cout << endl;
   //list.iterator( ); 

   // get_first() test 1
   cout << "First Node: " << list.get_first() << endl;

   // get_next() test
   cout << "Next Node: " << list.get_next( list.get_first() ) << endl;
   //cout << "Node after nullptr: " << list.get_next( (Node*)nullptr ) << endl << endl;

   // pop_front() and size() test
   list.pop_front();
   list.pop_front();
   list.pop_front();
   list.pop_front();
   cout << boolalpha << list.empty() << endl;
   cout << "size: " << list.size() << endl;

   // get_first() test 2
   cout << "First Node: " << list.get_first() << endl;

/* ///////////////////////////////////// ANIMAL FARM 3 /////////////////////////////////////////////////
   list<Animal*> animalList;
   for( int i = 0 ; i < 25 ; i++ ) {
      animalList.push_front( AnimalFactory::getRandomAnimal() );
	}

   cout << endl << "List of Animals: "      << endl;
   cout <<         "   Is it empty: "        << boolalpha << animalList.empty() << endl;
   cout <<         "   Number of elements: " << animalList.size() << endl;
   cout <<         "   Max size: "           << animalList.max_size() << endl;

   for ( Animal* pAnimal : animalList ) {   // range-based for loop
      if ( pAnimal == NULL ) break;          // check if the end of the array elements has been reached
      cout << pAnimal->speak() << endl;
   }
   
   for ( Animal* pAnimal : animalList )   delete pAnimal;
   for( int i = 0 ; i < 25 ; i++ )        animalList.pop_front();
   cout << endl;
//////////////////////////////////////////////////////////////////////////////////////////////////// */

   return 0;
}
