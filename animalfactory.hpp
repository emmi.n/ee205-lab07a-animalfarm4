///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file animalfactory.hpp
/// @version 1.0
///
/// Contains class declaration for animalfactory.cpp
///
/// @author EmilyPham <emilyn3@hawaii.edu>
/// @brief  Lab 07a - Animal Farm 4 - EE 205 - Spr 2021
/// @date   25_MAR_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "animal.hpp"

using namespace std;

namespace animalfarm {

#define MAX_WEIGHT (30)    // maximum weight of an Aku
#define MIN_WEIGHT (5)   // minimum weight of an Aku

class AnimalFactory {
   public:
      static Animal* getRandomAnimal();
};

} //namespace animalfarm
