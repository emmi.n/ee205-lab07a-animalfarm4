///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file node.cpp
/// @version 1.0
///
/// Exports data about all nodes
///
/// @author EmilyPham <emilyn3@hawaii.edu>
/// @brief  Lab 07a - Animal Farm 4 - EE 205 - Spr 2021
/// @date   25_MAR_2021
///////////////////////////////////////////////////////////////////////////////

/*    @note this is just for documentation purposes (node.cpp is empty)    */
