///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file list.hpp
/// @version 1.0
///
/// Exports data about all lists
///
/// @author EmilyPham <emilyn3@hawaii.edu>
/// @brief  Lab 07a - Animal Farm 4 - EE 205 - Spr 2021
/// @date   29_MAR_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "node.hpp"

namespace containers {

class SingleLinkedList {
private:
   unsigned int numberOFnodes = 0;

protected:
	Node* head = nullptr;

public:
   void iterator() const;  // an iterator used for testing only (depends on DEBUG)
   
   const bool empty() const;           // @note I changed order to group methods by "type"
   unsigned int size() const;
   
   void  push_front( Node* newNode );
   Node* pop_front();
   
   Node* get_first() const;
   Node* get_next( const Node* currentNode ) const;
};

} // namespace containers
