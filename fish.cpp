///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file fish.cpp
/// @version 1.0
///
/// Exports data about all fish
/// 
/// @author Emily_Pham <emilyn3@hawaii.edu>
/// @brief  Lab 07a - Animal Farm 4 - EE 205 - Spr 2021
/// @date   25_MAR_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "fish.hpp"

using namespace std;

namespace animalfarm {

void Fish::printInfo() {
   Animal::printInfo();
   cout << "   Scale Color = [" << colorName( scaleColor ) << "]" << endl;
	cout << "   Favorite Temperature = [" << favoriteTemperature << "]" << endl;
}

const string Fish::speak() {
	return string( "Bubble bubble" );
}

} // end namespace animalfarm
