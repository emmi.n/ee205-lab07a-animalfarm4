///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file nunu.hpp
/// @version 1.0
///
/// Exports data about all nunu fish
///
/// @author Emily_Pham <emilyn3@hawaii.edu>
/// @brief  Lab 07a - Animal Farm 4 - EE 205 - Spr 2021
/// @date   25_MAR_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

#include "fish.hpp"

using namespace std;


namespace animalfarm {

class Nunu : public Fish {
public:
   bool isNative;
   
   Nunu( bool isNative, enum Color newColor, enum Gender newGender );
	
	void printInfo();
};

} // namespace animalfarm
