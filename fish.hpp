///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file fish.hpp
/// @version 1.0
///
/// Exports data about all fish
///
/// @author Emily_Pham <emilyn3@hawaii.edu>
/// @brief  Lab 07a - Animal Farm 4 - EE 205 - Spr 2021
/// @date   25_MAR_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "animal.hpp"

namespace animalfarm {

class Fish : public Animal {
public:
   enum Color  scaleColor;
   float       favoriteTemperature;

   void printInfo();

	virtual const string speak();
};

} //namespace animalfarm
