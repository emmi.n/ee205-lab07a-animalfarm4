///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file bird.cpp
/// @version 1.0
///
/// Exports data about all birds
///
/// @author EmilyPham <emilyn3@hawaii.edu>
/// @brief  Lab 07a - Animal Farm 4 - EE 205 - Spr 2021
/// @date   25_MAR_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "bird.hpp"

using namespace std;

namespace animalfarm {

void Bird::printInfo() {
   Animal::printInfo();
   cout << "   Feather Color = [" << colorName( featherColor ) << "]" << endl;
	cout << "   Is Migratory = [" << boolalpha << isMigratory << "]" << endl;
}

const string Bird::speak() {
	return string( "Tweet" );
}

} // end namespace animalfarm
