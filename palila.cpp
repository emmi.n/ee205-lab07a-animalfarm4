///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file palila.cpp
/// @version 1.0
///
/// Exports data about all palila birds
///
/// @author Emily_Pham <emilyn3@hawaii.edu>
/// @brief  Lab 07a - Animal Farm 4 - EE 205 - Spr 2021
/// @date   25_MAR_2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "palila.hpp"

using namespace std;

namespace animalfarm {
	
Palila::Palila ( string newWhereFound, enum Color newColor, enum Gender newGender ) {
	whereFound = newWhereFound;
   gender = newGender;         
	species = "Loxioides bailleui";
	featherColor = newColor;
   isMigratory = false;              /// is-a relationship
}


void Palila::printInfo() {
   cout << "Palila" << endl;
   cout << "   Where Found = [" << whereFound  << "]" << endl;
	Bird::printInfo();
}

} // namespace animalfarm
